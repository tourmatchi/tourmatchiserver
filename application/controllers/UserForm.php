<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/Auth_Controller.php';

class UserForm extends Auth_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_form_model');
        $this->load->model('auth_model');
    }

    function userforms_get()
    {
        $this->check_admin();

        $result = $this->user_form_model->get_all();
        $this->response(
            array('data' => $result)
        );
    }

    function userforms_post()
    {
        $this->check_member();

        $access_token = $this->input->get_request_header('Authorization');
        $account = $this->auth_model->get_account($access_token);

        $age0_12 = $this->input->post('age0_12');
        $age13_18 = $this->input->post('age13_18');
        $age19_35 = $this->input->post('age19_35');
        $age36_50 = $this->input->post('age36_50');
        $age51_65 = $this->input->post('age51_65');
        $age65up = $this->input->post('age65up');
        $attraction = $this->input->post('attraction');
        $special = $this->input->post('special');
        $topics = $this->input->post('topics');
        $city = $this->input->post('city');
        $date = $this->input->post('date');

        $userForm = array(
            'user_id' => $account->user_id,
            'age0_12' => $age0_12,
            'age13_18' => $age13_18,
            'age19_35' => $age19_35,
            'age36_50' => $age36_50,
            'age51_65' => $age51_65,
            'age65up' => $age65up,
            'attraction' => $attraction,
            'special' => $special,
            'city' => $city,
            'date' => $date
        );
        $result = $this->user_form_model->insert_userform($userForm);
        $userFormId = $result['id'];

        if (!empty($topics)) {
            $this->user_form_model->insert_form_choose_topics($userFormId, $topics);
        }

        $userForm = $this->user_form_model->get_user_form($userFormId);
        $this->response($userForm);
    }
}