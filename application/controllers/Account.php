<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Account extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("account_model");
    }

    public function register_post()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');

        try {
            $result = $this->account_model->register_account($email, $password, $name);
            $this->response($result);
        } catch (Exception $exception) {
            $this->output->set_header("HTTP/1.1 403 Forbidden");
            $this->response(array('error' => $exception->getMessage()));
        }
    }

    public function confirm_get()
    {
        $confirm_token = $this->query('confirm_token');

        if ($confirm_token != '' && $this->account_model->confirm($confirm_token)) {
            $account = $this->account_model->get_account(array('confirm_token' => $confirm_token), false);
            $this->response($account);
        } else {
            $this->output->set_header("HTTP/1.1 403 Forbidden");
            $this->response('not found');
        }
    }
}
