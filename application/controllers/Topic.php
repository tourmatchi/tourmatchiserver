<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/Auth_Controller.php';

class Topic extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('topic_model');
        $this->load->model('auth_model');
    }

    public function topics_post()
    {
        $this->check_admin();

        $type = $this->input->post('type');

        $access_token = $this->input->get_request_header('Authorization');
        $account = $this->auth_model->get_account($access_token);
        $topic = array(
            'type' => $type,
        );
        $result = $this->topic_model->insert_topic($topic);
        $this->response($result);
    }

    public function topics_get()
    {
        $result = $this->topic_model->get_all();
        $this->response(
            $result
        );
    }
}