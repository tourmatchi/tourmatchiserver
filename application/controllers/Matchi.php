<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/Auth_Controller.php';

class Matchi extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("matchi_model");
    }

    public function matchii_get()
    {
        $matchi = $this->matchi_model->get_all();
        $this->response($matchi);
    }

    public function matchii_post()
    {
        $this->check_admin();

        $matchi_data = array(
            'image' => $_FILES['image_file'],
            'city' => $this->post('city'),
            'name' => $this->post('name')
        );
        $result = $this->matchi_model->insert_matchi($matchi_data);
        $this->response($result);
    }
}
