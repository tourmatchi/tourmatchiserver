<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/Auth_Controller.php';

class Service extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("service_model");
    }

    public function services_get()
    {
        $services = $this->service_model->get_all();
        $this->response($services);
    }

    public function services_post()
    {
        $this->check_admin();

        $service_data = array(
            'image' => $_FILES['image_file'],
            'title' => $this->post('title'),
            'content' => $this->post('content')
        );
        $result = $this->service_model->insert_service($service_data);
        $this->response($result);
    }
}
