<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/Auth_Controller.php';

class Play extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("play_model");
    }

    public function plays_get()
    {
        $plays = $this->play_model->get_all();
        $this->response($plays);
    }

    public function plays_post()
    {
        $this->check_admin();

        $play_data = array(
            'image' => $_FILES['image_file'],
            'description' => $this->post('description'),
            'city' => $this->post('city'),
            'url' => $this->post('url')
        );
        $result = $this->play_model->insert_play($play_data);
        $this->response($result);
    }
}