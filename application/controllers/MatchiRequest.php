<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/Auth_Controller.php';

class MatchiRequest extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('matchi_request_model');
        $this->load->model('auth_model');
    }

    public function matchirequests_post()
    {
        $this->check_member();

        $city = $this->input->post('city');

        $access_token = $this->input->get_request_header('Authorization');
        $account = $this->auth_model->get_account($access_token);
        $matchi_request = array(
            'user_id' => $account->user_id,
            'city' => $city
        );
        $result = $this->matchi_request_model->insert_matchi_request($matchi_request);
        $this->response($result);
    }

    public function matchirequests_get()
    {
        $this->check_admin();

        $result = $this->matchi_request_model->get_all();
        $this->response(
            $result
        );
    }
}
