<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/Auth_Controller.php';

class Slide extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("slide_model");
    }

    public function slides_get()
    {
        $slides = $this->slide_model->get_all();
        $this->response($slides);
    }

    public function slides_post()
    {
        $this->check_admin();

        $result = $this->slide_model->insert_slide(array('image' => $_FILES['image_file']));
        $this->response($result);
    }

    public function slides_delete()
    {
        $id = $this->query('id');
        $this->output->append_output($id);
        $this->slide_model->delete_slide($id);
    }
}