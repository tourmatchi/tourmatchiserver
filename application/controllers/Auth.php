<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Auth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("auth_model");
    }

    public function login_post()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        if ($this->auth_model->login($email, $password)) {
            $auth = $this->auth_model->generate_auth($email);
            $this->response($auth);
        } else {
            $this->output->set_header("HTTP/1.1 403 Forbidden");
        }
    }
}