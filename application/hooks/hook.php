<?php

class Hook
{
    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function EnableCrossDomain()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: Authorization, Access-Control-Request-Method, Access-Control-Request-Headers");

        //CORS preflight
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            $this->CI->response(array('CORS' => 'options'));
        }
    }
}
