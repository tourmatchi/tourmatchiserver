<?php

class Slide_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("imgur_uploader");
    }

    public function get_all()
    {
        $query = $this->db->get('slide');
        return $query->result();
    }

    public function insert_slide($slide_data)
    {
        $url = $this->imgur_uploader->upload($slide_data['image']);
        $slide = array('image_url' => $url);

        $this->db->insert('slide', $slide);
        $result = $slide;
        $result['id'] = $this->db->insert_id();
        return $result;
    }

    public function delete_slide($id)
    {
        $this->db->delete('slide', array('slide_id' => $id));;
    }
}
