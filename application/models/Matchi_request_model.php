<?php

class Matchi_request_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $query = $this->db->get('matchi_request');
        return $query->result();
    }

    public function insert_matchi_request($matchi_request)
    {
        $this->db->where('user_id',$matchi_request['user_id']);
        $query = $this->db->get('account');
        $query = $query->result_array();

        $this->load->library('slack_sender');
        $slack_request = 
                'name:'.$query[0]['name']."\n".
                'email:'.$query[0]['email']."\n".
                'city:'.$matchi_request['city']."\n".
                'user_id:'.$matchi_request['user_id'];
        $this->slack_sender->sendslack($slack_request,"C0VKUBC80");

        $this->db->insert('matchi_request', $matchi_request);
        $result = $matchi_request;
        $result['id'] = $this->db->insert_id();
        return $result;
    }

    public function delete_matchi_request($id)
    {
        $this->db->delete('matchi_request', array('request_id' => $id));;
    }
}