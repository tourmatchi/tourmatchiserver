<?php

class Matchi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("imgur_uploader");
    }

    public function get_all()
    {
        $query = $this->db->get('matchi');
        return $query->result();
    }

    public function insert_matchi($matchi_data)
    {
        $url = $this->imgur_uploader->upload($matchi_data['image']);
        $matchi = array(
            'image_url' => $url,
            'city' => $matchi_data['city'],
            'name' => $matchi_data['name']
        );
        $this->db->insert('matchi', $matchi);
        $result = $matchi;
        $result['id'] = $this->db->insert_id();
        return $result;
    }

    public function delete_matchi($id)
    {
        $this->db->delete('matchi', array('matchi_id' => $id));;
    }
}