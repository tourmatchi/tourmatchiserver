<?php

class User_form_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $this->db->select('userform_id');
        $this->db->from('userform');
        $queryUserForms = $this->db->get()->result();
        $userForms = array();
        foreach ($queryUserForms as $userForm) {
            array_push($userForms, $this->get_user_form($userForm->userform_id));
        }
        return $userForms;
    }

    public function insert_userform($userform)
    {
        $this->db->insert('userform', $userform);
        $result = $userform;
        $result['id'] = $this->db->insert_id();

        $this->db->where('user_id',$userform['user_id']);
        $query = $this->db->get('account');
        $query = $query->result_array();

        $this->load->library('slack_sender');
        $slack_request = 
            'userform_id:'.$result['id'].','.
            'name:'.$query[0]["name"].','.
            'email:'.$query[0]["email"].','.
            'user_id:'.$userform['user_id'].','.
            'age0～12:'. $userform['age0_12'].','.
            'age13～18:'. $userform['age13_18'].','.
            'age19～35:'. $userform['age19_35'].','.
            'age36～50:'. $userform['age36_50'].','.
            'age51~65:'. $userform['age51_65'].','.
            'age65up:'. $userform['age65up'].','.
            'attraction:'. $userform['attraction'].','.
            'special:'. $userform['special'].','.
            'city:'. $userform['city'].','.
            'date:'. $userform['date'];
            
        $this->slack_sender->sendslack($slack_request,'C0VKTMA4V');
        return $result;
    }

    public function insert_form_choose_topics($userFormId, $topics)
    {
        foreach ($topics as $topicId) {
            $formChooseTopic = array(
                'userform_id' => $userFormId,
                'topic_id' => $topicId);
            $this->db->insert('form_choose_topic', $formChooseTopic);
            $this->load->library('slack_sender');
            switch ($topicId) {
                case 1:
                    $topicdata[] = '歷史';
                    break;
                case 2:
                    $topicdata[] = '建築';
                    break;
                case 3:
                    $topicdata[] = '藝術';
                    break;
                case 4:
                    $topicdata[] = '美食';
                    break;
                case 5:
                    $topicdata[] = '購物';
                    break;
                case 6:
                    $topicdata[] = '自然景觀';
                    break;
                case 7:
                    $topicdata[] = '戶外活動';
                    break;    
            }    
        
        }
        $slack_request = 'userform_id:'.$userFormId.'topics:'.implode(",",$topicdata);
        $this->slack_sender->sendslack($slack_request,'C0VKTMA4V');

    }

    public function get_user_form($userFormId)
    {
        $query = $this->db->get_where('userform', array('userform_id' => $userFormId));
        if ($query->num_rows() > 0) {
            $userForm = $query->row();
            $topics = $this->db->get_where('form_choose_topic', array('userform_id' => $userFormId))->result();
            $userForm->topics = array();
            foreach ($topics as $topic) {
                array_push($userForm->topics, $topic->topic_id);
            }
            return $userForm;
        }
        return null;
    }

    public function delete_userform($id)
    {
        $this->db->delete('userform', array('userform_id' => $id));;
    }
}