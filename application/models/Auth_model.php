<?php

require APPPATH . '/models/Role.php';

class Auth_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper("tourmatchi");
    }

    public function login($email, $password)
    {
        $encrypt_password = tm_encode($password);
        $query = $this->db->get_where('account', array('email' => $email, 'password' => $encrypt_password, 'active' => true));
        return $query->num_rows() > 0;
    }

    function generate_auth($email)
    {
        $query = $this->db->get_where('account', array('email' => $email));

        if ($query->num_rows() > 0) {
            $account = $query->row();
            $access_token = tm_uniqid();
            $renew_token = tm_uniqid();
            $expire_time = date('Y-m-d', tm_week_seconds());
            $this->db->where('email', $email);
            $this->db->update('account', array(
                'access_token' => $access_token,
                'renew_token' => $renew_token,
                'expire_time' => $expire_time
            ));

            return array(
                'id' => $account->user_id,
                'role' => $account->role,
                'name' => $account->name,
                'email' => $email,
                'access_token' => $access_token,
                'renew_token' => $renew_token,
                'expire_time' => $expire_time
            );
        }
        return array();
    }

    function is_member($access_token)
    {
        return $this->is_role($access_token, Role::Member);
    }

    function is_admin($access_token)
    {
        return $this->is_role($access_token, Role::Admin);
    }

    function is_role($access_token, $role)
    {
        $query = $this->db->get_where('account', array('access_token' => $access_token, 'expire_time >=' => 'now()'));
        if ($query->num_rows() > 0) {
            $account = $query->row();
            return $account->role == $role;
        }
        return false;
    }

    function get_account($access_token)
    {
        $query = $this->db->get_where('account', array('access_token' => $access_token));
        $account = $query->row();
        return $account;
    }
}
