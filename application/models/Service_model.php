<?php

class Service_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("imgur_uploader");
    }

    public function get_all()
    {
        $query = $this->db->get('service');
        return $query->result();
    }

    public function insert_service($service_data)
    {
        $url = $this->imgur_uploader->upload($service_data['image']);
        $service = array(
            'image_url' => $url,
            'title' => $service_data['title'],
            'content' => $service_data['content']
        );

        $this->db->insert('service', $service);
        $result = $service;
        $result['id'] = $this->db->insert_id();
        return $result;
    }
}

