<?php

class Play_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("imgur_uploader");
    }

    public function get_all()
    {
        $query = $this->db->get('play');
        return $query->result();
    }

    public function insert_play($play_data)
    {
        $url = $this->imgur_uploader->upload($play_data['image']);
        $play = array(
            'image_url' => $url,
            'description' => $play_data['description'],
            'city' => $play_data['city'],
            'url' => $play_data['url']
        );

        $this->db->insert('play', $play);
        $result = $play;
        $result['id'] = $this->db->insert_id();
        return $result;
    }
}