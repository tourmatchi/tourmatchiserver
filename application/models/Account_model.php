<?php

require APPPATH . '/models/Role.php';

class Account_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("tourmatchi");
    }

    public function register_account($email, $password, $name)
    {
        $query = $this->db->get_where('account', array('email' => $email));
        if ($query->num_rows() == 0) {
            $confirm_token = tm_uniqid();
            $account = array(
                'email' => $email,
                'password' => tm_encode($password),
                'name' => $name,
                'role' => Role::Member,
                'confirm_token' => $confirm_token,
                'active' => false
            );
            $this->db->insert('account', $account);
            $result = array(
                'user_id' => $this->db->insert_id(),
                'email' => $account['email'],
                'name' => $account['name'],
                'role' => $account['role'],
                'active' => $account['active']);
            $this->send_confirm_email($email, $name, $confirm_token);
        } else {
            $account = $query->row();
            if ($account->active == 'f') {
                $this->send_confirm_email($account->email, $account->name, $account->confirm_token);
                return $account;
            }
            throw new Exception("Email is exist.");
        }
        return $result;
    }

    private function send_confirm_email($email, $name, $confirm_token)
    {
        $this->load->library('email_sender');
        $title = 'TourMatchi帳號認證';
        $confirm_token_full = 'http://www.tourmatchi.com/#confirm?confirmToken='.$confirm_token;
        $message = 
        '哈囉'.$name.
        '<br><br>'.
        '我是TourMatchi的創辦人'.'<i>C.H.</i>'.'，歡迎你加入TourMatchi旅遊顧問平台！'.'<br><br>'.
        '<a href = '.$confirm_token_full.'>啟用帳號，開始一趟專屬於你的旅程</a>'.'<br><br>'.
        '<br><br><img src="http://i.imgur.com/kheTpdy.jpg?1"/><br><br>'.
        '這是一個由一群旅行癡共同打造的空間，為你創造最棒的旅遊體驗。'.'<br>'.
        '不同於一般的旅遊網站，在TourMatchi你可以享受以下服務：'
        .'<br>'.
        '<strong>1. 客製化的旅行行程規劃 </strong><br>'.
        '只要三分鐘，將你的目的地、旅行日期和特殊需求填入表單，靜候三到五日，即可獲得當地旅遊顧問為你量身打造的行程。'.
        '<br><strong>2. 獨一無二的海外城市情報</strong><br>'.
        '千萬不能錯過最麻吉的海外顧問撰寫的旅行故事！進入TourMatchi的Wordpress部落格，為下一次旅行蒐集素材以及靈感吧！'.
        '<br><strong>3. 最懂你的旅遊平台</strong><br>'.
        '找不到理想的顧問、想請顧問幫忙檢查你的行程或是對於行程還有任何疑問，都歡迎到TourMatchi粉絲專頁傳訊息給我們，從頭到腳搞定出發之前的疑難雜症！如果有任何問題，歡迎回信給我們:)現在，趕快回到我們的網站，開啟一趟前所未有的旅程吧！'.
        '<br><br>'.
        'Bon voyage,'.
        '<br><br>'.
        'TourMatchi 創辦人'.'<i>C.H.</i>'.
        '<br><br><br>'.
        '——————'.'<br>'.
        '官方網站: www.tourmatchi.com'.'<br>'.
        '粉絲專頁: www.facebook.com/tourmatchi'.'<br>'.
        '官方部落格: tourmatchi.wordpress.com'.'<br>'.
        '<img src = "http://i.imgur.com/lbXUvGI.png?1" height = "150" width = "150">';

        $this->email_sender->send($email, $title, $message);
    }

    public function get_account($query, $showPrivate = false)
    {
        $query = $this->db->get_where('account', $query);
        if ($query->num_rows() > 0) {
            $account = $query->row();
            if ($showPrivate) {
                return $account;
            } else {
                $account = array(
                    'email' => $account->email,
                    'name' => $account->name,
                    'role' => $account->role);
                return $account;
            }
        }
        return array();
    }

    public function confirm($confirm_token)
    {
        $query = $this->db->get_where('account', array('confirm_token' => $confirm_token));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($row->active == 't') {
                return false;
            }
            $this->db->where('confirm_token', $confirm_token);
            $this->db->update('account', array('active' => true));
            return true;
        }
        return false;
    }
}