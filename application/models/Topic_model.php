<?php

class Topic_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $query = $this->db->get('topic');
        return $query->result();
    }

    public function insert_topic($topic)
    {
        $this->db->insert('topic', $topic);
        $result = $topic;
        $result['id'] = $this->db->insert_id();
        return $result;
    }
}
