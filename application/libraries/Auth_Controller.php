<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Auth_Controller extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("auth_model");
    }

    protected function check_admin()
    {
        $this->check_role(Role::Admin);
    }

    protected function check_member()
    {
        $this->check_role(Role::Member);
    }

    private function check_role($role)
    {
        $access_token = $this->input->get_request_header('Authorization');
        if (!$this->auth_model->is_role($access_token, $role)) {
            $this->output->set_header("HTTP/1.1 403 Forbidden");
            $this->output->_display();
            exit();
        }
    }
}
