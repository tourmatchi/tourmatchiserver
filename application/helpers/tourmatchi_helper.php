<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('tm_encode')) {
    function tm_encode($data)
    {
        $encrypt_data = hash('md5', $data);
        return $encrypt_data;
    }
}

if (!function_exists('tm_uniqid')) {
    function tm_uniqid()
    {
        return uniqid('tourmatchi', true);
    }
}

if (!function_exists('tm_week_seconds')) {
    function tm_week_seconds()
    {
        date_default_timezone_set('Asia/Taipei');//設定時區
        $week_seconds = time() + (24 * 60 * 60 * 7);//有效期限為token生成時間+七天
        return $week_seconds;
    }
}

if (!function_exists('tm_now')) {
    function tm_now()
    {
        date_default_timezone_set('Asia/Taipei');//設定時區
        $week_seconds = time() + (24 * 60 * 60 * 7);//有效期限為token生成時間+七天
        return $week_seconds;
    }
}
